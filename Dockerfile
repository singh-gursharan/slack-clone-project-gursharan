From python:3.6

WORKDIR /app

COPY . /app

RUN pip3 install psycopg2

RUN pip3 install -r requirements.txt

EXPOSE 5000

# CMD flask run --host=0.0.0.0

CMD ["flask", "run", "--host=0.0.0.0"] 